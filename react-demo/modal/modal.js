import React from 'react'
import Form from '../form/form'
import './modal.css'

export default class Modal extends React.Component {
  render () {
    return (
      <div className='Modal'>
        <div className='Modal-body'>
          <a className='Modal-closeButton'>X</a>
          <Form />
        </div>
      </div>
    )
  }
}
