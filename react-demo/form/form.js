import React from 'react'
import './form.css'

export default class Form extends React.Component {
  render () {
    return (
      <form className='Form'>
        <ul className='Form-inputList'>
          <li className='Form-inputListItem'>
            <label>
              Name
              <input className='Form-input' type='text'/>
            </label>
          </li>
          <li className='Form-inputListItem'>
            <label>
              Email
              <input className='Form-input' type='email'/>
            </label>
          </li>
        </ul>
        <div className='Form-footer'>
          <button className='Form-submit'>Submit</button>
        </div>
      </form>
    )
  }
}
