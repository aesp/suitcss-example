import React from 'react'
import { render } from 'react-dom'
import Modal from './modal/modal'

var container = document.createElement('div')
container.className = 'ModalContainer'
container.style.height = '100%'
document.body.appendChild(container)

render(<Modal />, container)
