import React from 'react'
import styles from './form.css'

export default class Form extends React.Component {
  render () {
    return (
      <form className={styles.Form}>
        <ul className={styles['Form-inputList']}>
          <li className={styles['Form-inputListItem']}>
            <label>
              Name
              <input className={styles['Form-input']} type='text'/>
            </label>
          </li>
          <li className={styles['Form-inputListItem']}>
            <label>
              Email
              <input className={styles['Form-input']} type='email'/>
            </label>
          </li>
        </ul>
        <div className={styles['Form-footer']}>
          <button className={styles['Form-submit']}>Submit</button>
        </div>
      </form>
    )
  }
}
