import React from 'react'
import Form from '../form/form'
import styles from './modal.css'

export default class Modal extends React.Component {
  render () {
    return (
      <div className={styles.Modal}>
        <div className={styles['Modal-body']}>
          <a className={styles['Modal-closeButton']}>X</a>
          <Form />
        </div>
      </div>
    )
  }
}
