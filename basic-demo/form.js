import _ from 'lodash'
import template from './form.html'
import './form.css'

module.exports = function createFormModal () {
  var el = document.createElement('div')
  el.className = 'ModalContainer'
  el.style.height = '100%'
  el.innerHTML = _.template(template)()
  el.querySelector('button').addEventListener('click', e => e.preventDefault())
  return el
}

