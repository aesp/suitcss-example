var path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: {
    bundle: './basic-demo/index.js',
    react: './react-demo/index.js',
    modules: './css-modules-demo/index.js'
  },
  output: {
    path: path.resolve(__dirname),
    publicPath: 'http://localhost:8080/demo/',
    filename: '[name].js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel'],
      exclude: /node_modules/
    }, {
      test: /\.html$/,
      loaders: ['text'],
      exclude: /node_modules/
    }, {
      test: /\.css$/,
      loader: 'style!css',
      exclude: /node_modules|css\-modules\-demo/
    }, {
      test: /\.css$/,
      loader: 'style!css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss',
      include: /css\-modules\-demo/
    }]
  },
  postcss: function () {
    return [
      require('postcss-cssnext')
    ]
  }
}
